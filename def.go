package httparg

import (
	"github.com/bcowtech/arg"
)

type InvalidArgumentError = arg.InvalidArgumentError
