package httparg

import (
	"sync"

	"github.com/bcowtech/httparg/form"
	"github.com/bcowtech/httparg/jsoncontent"
)

type Validatable interface {
	Validate() error
}

type ContentProcessFunc func(data []byte, v interface{}) error

type ErrorHandleFunc func(err error)

var (
	errorHandleFuncOnce sync.Once
	errorHandleFunc     ErrorHandleFunc

	builtinContentProcessors = map[string]ContentProcessFunc{
		"application/x-www-form-urlencoded": form.Process,
		"application/json":                  jsoncontent.Process,
	}
)

func CurrentErrorHandleFunc() ErrorHandleFunc {
	innerSetUpErrorHandleFunc(nil)
	return errorHandleFunc
}

func SetUpErrorHandleFunc(fn ErrorHandleFunc) {
	if errorHandleFunc != nil {
		panic("[httparg] cannot setup global ErrorHandleFunc")
	}
	innerSetUpErrorHandleFunc(fn)
}

func PanicErrorHandlerFunc(err error) { panic(err) }

func Args(v interface{}) *Processor {
	return &Processor{
		v: v,
	}
}

func GetContentProcessFunc(contentType string) ContentProcessFunc {
	if builtinContentProcessors != nil {
		processor, ok := builtinContentProcessors[contentType]
		if ok {
			return processor
		}
	}
	return nil
}

func RegisterContentProcessFunc(contentType string, processFunc ContentProcessFunc) bool {
	if builtinContentProcessors != nil {
		if processFunc == nil {
			delete(builtinContentProcessors, contentType)
		} else {
			builtinContentProcessors[contentType] = processFunc
		}
		return true
	}
	return false
}

func innerSetUpErrorHandleFunc(fn ErrorHandleFunc) {
	errorHandleFuncOnce.Do(func() {
		if fn == nil {
			errorHandleFunc = PanicErrorHandlerFunc
		} else {
			errorHandleFunc = fn
		}
	})
}
