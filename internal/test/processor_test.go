package test

import (
	"bytes"
	"fmt"
	"reflect"
	"testing"

	"github.com/bcowtech/httparg"
	"github.com/bcowtech/structprototype"
)

type DummyRequestArg struct {
	ID          string                 `json:"*id"`
	Type        *string                `json:"*type"`
	Number      int64                  `json:"number"`
	ShowDetail  bool                   `json:"-"        query:"SHOW_DETAIL"`
	EnableDebug bool                   `json:"-"        query:"ENABLE_DEBUG"`
	Tags        []string               `json:"tags"`
	Detail      *DummyRequestArgDetail `json:"detail"`
}

type DummyRequestArgDetail struct {
	Operator string `json:"operator"`
}

type DummyRequestArgValidatable DummyRequestArg

func (v *DummyRequestArgValidatable) Validate() error {
	if v.ID == "0" {
		return &httparg.InvalidArgumentError{
			Name:   "id",
			Reason: "cannot be 0",
		}
	}
	return nil
}

func TestProcessor(t *testing.T) {
	query := "SHOW_DETAIL"
	postbody := []byte(`
	{
		"id": "F0003452",
		"type": "KNNS",
		"number": 280123412341234123,
		"tags": ["T","ER","XVV"],
		"detail": {
			"operator": "nami"
		}
	}`)
	contentType := "  application/json ;charset=utf8"
	arg := DummyRequestArg{}
	processor := httparg.NewProcessor(&arg, httparg.ProcessorOption{
		ErrorHandleFunc: func(err error) {
			t.Error(err)
		},
	})

	processor.
		ProcessQueryString(query).
		ProcessPostBody(postbody, contentType)

	if arg.ID != "F0003452" {
		t.Errorf("assert 'DummyRequestArg.ID':: expected '%v', got '%v'", "F0003452", arg.ID)
	}
	if *arg.Type != "KNNS" {
		t.Errorf("assert 'DummyRequestArg.Type':: expected '%v', got '%v'", "KNNS", arg.Type)
	}
	if arg.Number != 280123412341234123 {
		t.Errorf("assert 'DummyRequestArg.Number':: expected '%v', got '%v'", 280123412341234123, arg.Number)
	}
	if arg.ShowDetail != true {
		t.Errorf("assert 'DummyRequestArg.ShowDetail':: expected '%v', got '%v'", true, arg.ShowDetail)
	}
	if arg.EnableDebug != false {
		t.Errorf("assert 'DummyRequestArg.EnableDebug':: expected '%v', got '%v'", false, arg.EnableDebug)
	}
	expectedTags := []string{"T", "ER", "XVV"}
	if !reflect.DeepEqual(arg.Tags, expectedTags) {
		t.Errorf("assert 'character.Alias':: expected '%#v', got '%#v'", expectedTags, arg.Tags)
	}
	{
		if arg.Detail == nil {
			t.Error("assert 'DummyRequestArg.Detail':: should not be nil")
		}
		var detail = arg.Detail
		if detail != nil {
			if detail.Operator != "nami" {
				t.Errorf("assert 'DummyRequestArg.Detail.Operator':: expected '%v', got '%v'", "nami", detail.Operator)
			}
		}
	}
}

func TestProcessNilValueOnRequiredField(t *testing.T) {
	defer func() {
		err := recover()
		if err == nil {
			t.Errorf("the 'TestProcessNilValueOnRequiredField()' should throw '%s' error", "missing required symbol 'type'")
		} else {
			missingRequiredFieldError, ok := err.(*structprototype.MissingRequiredFieldError)
			if !ok {
				t.Errorf("the error expected '%T', got '%T'", &structprototype.MissingRequiredFieldError{}, err)
			}
			if missingRequiredFieldError.Field != "type" {
				t.Errorf("assert 'MissingRequiredFieldError.Field':: expected '%v', got '%v'", "type", missingRequiredFieldError.Field)
			}
		}
	}()

	query := "SHOW_DETAIL"
	postbody := []byte(`
	{
		"id": "F0003452",
		"type": null,
		"number": 280123412341234123,
		"tags": ["T","ER","XVV"],
		"detail": {
			"operator": "nami"
		}
	}`)
	contentType := "  application/json ;charset=utf8"
	arg := DummyRequestArg{}
	processor := httparg.NewProcessor(&arg, httparg.ProcessorOption{
		ErrorHandleFunc: func(err error) {
			panic(err)
		},
	})

	processor.
		ProcessQueryString(query).
		ProcessPostBody(postbody, contentType)
}

func TestWrongContentType(t *testing.T) {
	defer func() {
		err := recover()
		if err == nil {
			t.Errorf("the 'TestWrongContentType()' should throw '%s' error", "cannot process specified content-type 'unknown_type'")
		}
	}()

	postbody := []byte("")
	contentType := "  unknown_type"

	arg := DummyRequestArg{}
	httparg.Args(&arg).
		ProcessPostBody(postbody, contentType)
}

func TestArgs(t *testing.T) {
	query := "SHOW_DETAIL"
	postbody := []byte(`
	{
		"id": "F0003452",
		"type": "KNNS",
		"number": 280123412341234123,
		"tags": ["T","ER","XVV"],
		"detail": {
			"operator": "nami"
		}
	}`)
	contentType := "  application/json ;charset=utf8"

	arg := DummyRequestArg{}
	httparg.Args(&arg).
		ProcessQueryString(query).
		ProcessPostBody(postbody, contentType)

	if arg.ID != "F0003452" {
		t.Errorf("assert 'DummyRequestArg.ID':: expected '%v', got '%v'", "F0003452", arg.ID)
	}
	if *arg.Type != "KNNS" {
		t.Errorf("assert 'DummyRequestArg.Type':: expected '%v', got '%v'", "KNNS", arg.Type)
	}
	if arg.Number != 280123412341234123 {
		t.Errorf("assert 'DummyRequestArg.Number':: expected '%v', got '%v'", 280123412341234123, arg.Number)
	}
	if arg.ShowDetail != true {
		t.Errorf("assert 'DummyRequestArg.ShowDetail':: expected '%v', got '%v'", true, arg.ShowDetail)
	}
	if arg.EnableDebug != false {
		t.Errorf("assert 'DummyRequestArg.EnableDebug':: expected '%v', got '%v'", false, arg.EnableDebug)
	}
	expectedTags := []string{"T", "ER", "XVV"}
	if !reflect.DeepEqual(arg.Tags, expectedTags) {
		t.Errorf("assert 'character.Alias':: expected '%#v', got '%#v'", expectedTags, arg.Tags)
	}
	{
		if arg.Detail == nil {
			t.Error("assert 'DummyRequestArg.Detail':: should not be nil")
		}
		var detail = arg.Detail
		if detail != nil {
			if detail.Operator != "nami" {
				t.Errorf("assert 'DummyRequestArg.Detail.Operator':: expected '%v', got '%v'", "nami", detail.Operator)
			}
		}
	}
}

func TestSetUpErrorHandleFunc(t *testing.T) {
	var buffer bytes.Buffer

	httparg.SetUpErrorHandleFunc(func(err error) {
		fmt.Fprintf(&buffer, "%+v", err)
	})
	query := "SHOW_DETAIL"
	postbody := []byte(`
	{
		"id": "F0003452",
		"type": null,
		"number": 280123412341234123,
		"tags": ["T","ER","XVV"],
		"detail": {
			"operator": "nami"
		}
	}`)
	contentType := "  application/json ;charset=utf8"

	if buffer.Len() != 0 {
		t.Errorf("buffer should be empty")
	}

	arg := DummyRequestArg{}
	httparg.Args(&arg).
		ProcessQueryString(query).
		ProcessPostBody(postbody, contentType)

	if buffer.Len() == 0 {
		t.Errorf("buffer should not be empty")
	}
	if buffer.String() != "missing required symbol 'type'" {
		t.Errorf("assert 'buffer.String()':: expected '%v', got '%v'", "missing required symbol 'type'", buffer.String())
	}
}

func TestValidatable(t *testing.T) {
	var buffer bytes.Buffer

	query := "SHOW_DETAIL"
	postbody := []byte(`
	{
		"id": "0",
		"type": "KNNS",
		"number": 280123412341234123,
		"tags": ["T","ER","XVV"],
		"detail": {
			"operator": "nami"
		}
	}`)
	contentType := "  application/json ;charset=utf8"

	if buffer.Len() != 0 {
		t.Errorf("buffer should be empty")
	}

	arg := DummyRequestArgValidatable{}
	httparg.NewProcessor(&arg, httparg.ProcessorOption{
		ErrorHandleFunc: func(err error) {
			fmt.Fprintf(&buffer, "%+v", err)
		},
	}).
		ProcessQueryString(query).
		ProcessPostBody(postbody, contentType).
		Validate()

	if buffer.Len() == 0 {
		t.Errorf("buffer should not be empty")
	}
	expectedErrMsg := "invalid argument 'id'; cannot be 0"
	if buffer.String() != expectedErrMsg {
		t.Errorf("assert 'buffer.String()':: expected '%v', got '%v'", expectedErrMsg, buffer.String())
	}
}
