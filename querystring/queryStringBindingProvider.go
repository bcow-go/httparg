package querystring

import (
	"net/url"
	"reflect"

	proto "github.com/bcowtech/structprototype"
)

type QueryStringBindingProvider struct {
	values url.Values
}

func NewQueryStringBindProvider(values url.Values) *QueryStringBindingProvider {
	instance := &QueryStringBindingProvider{
		values: values,
	}
	return instance
}

func (p *QueryStringBindingProvider) BeforeBind(context *proto.PrototypeContext) error {
	return nil
}

func (p *QueryStringBindingProvider) BindField(field proto.PrototypeField, rv reflect.Value) error {
	if v, ok := p.values[field.Name()]; ok {
		switch rv.Kind() {
		case reflect.Bool:
			if len(v[0]) == 0 {
				v[0] = True
			}
		}
		err := proto.StringArgsBinder(rv).Bind(v[0])
		if err != nil {
			return err
		}
	}
	return nil
}

func (p *QueryStringBindingProvider) AfterBind(context *proto.PrototypeContext) error {
	return context.CheckIfMissingRequiredFields(func() <-chan string {
		c := make(chan string, 1)
		go func() {
			for k, _ := range p.values {
				c <- k
			}
			close(c)
		}()
		return c
	})
}
