package querystring

import (
	"net/url"

	proto "github.com/bcowtech/structprototype"
)

const (
	TagName = "query"
	True    = "true"
)

func Process(query string, v interface{}) error {
	values, err := url.ParseQuery(query)
	if err != nil {
		return err
	}

	provider := NewQueryStringBindProvider(values)

	err = proto.Bind(v,
		&proto.PrototypifyConfig{
			TagName: TagName,
		}, provider)
	if err != nil {
		return err
	}

	return nil
}
