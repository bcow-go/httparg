package form

import (
	"net/url"

	"github.com/bcowtech/httparg/querystring"
	proto "github.com/bcowtech/structprototype"
)

const (
	TagName = "form"
	True    = "true"
)

func Process(data []byte, v interface{}) error {
	values, err := url.ParseQuery(string(data))
	if err != nil {
		return err
	}

	provider := querystring.NewQueryStringBindProvider(values)

	err = proto.Bind(v,
		&proto.PrototypifyConfig{
			TagName: TagName,
		}, provider)
	if err != nil {
		return err
	}

	return nil
}
