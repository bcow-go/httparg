package httparg

import (
	"strings"

	"github.com/bcowtech/httparg/querystring"
)

type Processor struct {
	v               interface{}
	hasError        bool
	errorHandleFunc ErrorHandleFunc
}

type ProcessorOption struct {
	ErrorHandleFunc ErrorHandleFunc
}

func NewProcessor(v interface{}, option ProcessorOption) *Processor {
	service := Processor{
		v:               v,
		errorHandleFunc: option.ErrorHandleFunc,
	}
	return &service
}

func (p *Processor) ProcessQueryString(query string) *Processor {
	if p.hasError {
		return p
	}

	var err error
	defer func() {
		if err != nil {
			p.throwError(err)
		}
	}()

	err = querystring.Process(query, p.v)
	return p
}

func (p *Processor) ProcessPostBody(data []byte, contentType string) *Processor {
	if p.hasError {
		return p
	}

	var err error
	defer func() {
		if err != nil {
			p.throwError(err)
		}
	}()

	contentType = p.extractContentType(contentType)
	processor := GetContentProcessFunc(contentType)
	if processor == nil {
		panic("[httparg] cannot process specified content-type '" + contentType + "'")
	}
	err = processor(data, p.v)
	return p
}

func (p *Processor) Process(data []byte, processFunc ContentProcessFunc) *Processor {
	if p.hasError {
		return p
	}

	var err error
	defer func() {
		if err != nil {
			p.throwError(err)
		}
	}()

	err = processFunc(data, p.v)
	return p
}

func (p *Processor) Validate() {
	if p.hasError {
		return
	}

	var err error
	defer func() {
		if err != nil {
			p.throwError(err)
		}
	}()

	v, ok := p.v.(Validatable)
	if ok {
		err = v.Validate()
	}
}

func (p *Processor) extractContentType(contentType string) string {
	i := strings.Index(contentType, ";")
	if i >= 0 {
		contentType = contentType[:i]
	}
	return strings.TrimSpace(contentType)
}

func (p *Processor) throwError(err error) {
	// set the hasError flag
	p.hasError = true

	errHandler := p.errorHandleFunc
	if errHandler == nil {
		errHandler = CurrentErrorHandleFunc()
	}
	if errHandler != nil {
		errHandler(err)
	}
}
