package jsoncontent

import (
	"fmt"
	"reflect"

	"github.com/bcowtech/structprototype/reflectutil"
)

var (
	assignZero = reflectutil.AssignZero
)

func indirect(v interface{}) (reflect.Value, error) {
	rv := reflect.ValueOf(v)

	if !rv.IsValid() {
		return reflect.Value{}, fmt.Errorf("specified argument 'v' is invalid")
	}

	for {
		switch rv.Kind() {
		case reflect.Ptr:
			if rv.IsNil() {
				rv = reflect.New(rv.Type().Elem())
			}
			rv = rv.Elem()
		default:
			return rv, nil
		}
	}
}
