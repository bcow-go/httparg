package jsoncontent

import (
	"bytes"
	"encoding/json"
)

const (
	TagName = "json"
)

func Process(data []byte, v interface{}) error {
	var content interface{}

	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.UseNumber()
	err := decoder.Decode(&content)
	if err != nil {
		return err
	}

	rv, err := indirect(v)
	if err != nil {
		return err
	}

	binder := buildJsonValueBinder(rv)
	err = binder.Bind(content)
	if err != nil {
		return err
	}

	return nil
}
