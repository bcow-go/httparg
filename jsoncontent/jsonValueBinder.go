package jsoncontent

import (
	"encoding/json"
	"fmt"
	"reflect"

	proto "github.com/bcowtech/structprototype"
	"github.com/bcowtech/structprototype/reflectutil"
)

var (
	typeOfJsonRawMessage = reflect.TypeOf(json.RawMessage(nil))
)

type jsonValueBinder reflect.Value

func (binder jsonValueBinder) Bind(content interface{}) error {
	rv := reflect.Value(binder)
	return binder.bindJsonValue(rv, content)
}

func (binder jsonValueBinder) bindJsonValue(rv reflect.Value, content interface{}) error {
	rv = reflect.Indirect(assignZero(rv))
	switch rv.Type() {
	case typeOfJsonRawMessage:
		// TODO create JsonRawMessageBinder !!!
		rv = reflect.Indirect(reflectutil.AssignZero(rv))
		b, err := json.Marshal(content)
		if err != nil {
			return &proto.ValueBindingError{
				Value: content,
				Kind:  rv.Type().Name(),
				Err:   err}
		}
		var raw = json.RawMessage(b)
		rv.Set(reflect.ValueOf(raw))
		return nil
	}
	scalarValueBinder := proto.ScalarBinder(rv)
	err := scalarValueBinder.Bind(content)
	if err != nil {
		switch rv.Kind() {
		case reflect.Array, reflect.Slice:
			jsonArray, ok := content.([]interface{})
			if !ok {
				return &proto.ValueBindingError{
					Value: jsonArray,
					Kind:  rv.Type().Name(),
					Err:   err}
			}
			return binder.bindJsonArray(rv, jsonArray)
		case reflect.Struct:
			jsonObject, ok := content.(map[string]interface{})
			if !ok {
				return &proto.ValueBindingError{
					Value: jsonObject,
					Kind:  rv.Type().Name(),
					Err:   err}
			}
			return binder.bindJsonObject(rv, jsonObject)
		case reflect.Map:
			return fmt.Errorf("not implemented")
		default:
			return err
		}
	}
	return nil
}

func (binder jsonValueBinder) bindJsonArray(rv reflect.Value, content []interface{}) error {
	if len(content) > 0 {
		size := len(content)
		container := reflect.MakeSlice(rv.Type(), size, size)
		for i, elem := range content {
			err := binder.bindJsonValue(container.Index(i), elem)
			if err != nil {
				return err
			}
		}
		rv.Set(container)
	}
	return nil
}

func (binder jsonValueBinder) bindJsonObject(rv reflect.Value, content map[string]interface{}) error {
	prototype, err := proto.Prototypify(rv,
		&proto.PrototypifyConfig{
			TagName:              TagName,
			BuildValueBinderFunc: buildJsonValueBinder,
		})
	if err != nil {
		return err
	}
	values := proto.Values(content)
	return prototype.BindValues(values)
}

func buildJsonValueBinder(rv reflect.Value) proto.ValueBinder {
	return jsonValueBinder(rv)
}
